import numpy as np
import cv2

paths = ['dublin_org.jpg', 'dublin_edited.jpg']
paths2 = ['budapest.jpg', 'bodo.jpeg', 'prague.jpg', 'rio.jpg', 'ny.jpg']
img_org = cv2.imread(paths[0])
img_edited = cv2.imread(paths[1])

_, threshed_img_edited = cv2.threshold(cv2.cvtColor(img_edited, cv2.COLOR_BGR2GRAY),
                112, 255, cv2.THRESH_BINARY)
_, threshed_img_org = cv2.threshold(cv2.cvtColor(img_org, cv2.COLOR_BGR2GRAY),
                112, 255, cv2.THRESH_BINARY)

subtraction_result = threshed_img_org - threshed_img_edited

kernel = np.ones((5,5), np.uint8)
img_dilation = cv2.dilate(subtraction_result, kernel, iterations=7)
img_erosion = cv2.erode(img_dilation, kernel, iterations=8)


contours, hier = cv2.findContours(img_erosion, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
maxc = max(contours, key=cv2.contourArea)
x, y, w, h = cv2.boundingRect(maxc)
kevin = img_edited[y:(y+h), x:(x+w), :]

img_edited_copy = img_edited.copy()
for c in contours:

    rect = cv2.minAreaRect(c)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    cv2.drawContours(img_edited_copy, [box], 0, (0, 255, 0), 1)

img_edited_copy = cv2.resize(img_edited_copy, (1280, 720))
#cv2.imshow('image',img_edited_copy)
#cv2.waitKey(0)
#cv2.imshow('Kevin`s face',kevin)
#cv2.waitKey(0)

lower_b = np.array([170, 202, 100])
upper_b = np.array([255, 255, 255])

for city in paths2:
    city_img = cv2.imread(city)
    hsv_city_img = cv2.cvtColor(city_img, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv_city_img, lower_b, upper_b)
    if paths2.index(city) != 0:
        mask = cv2.dilate(mask, kernel, iterations=4)
    contours2, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    maxc2 = max(contours2, key=cv2.contourArea)
    x2, y2, w2, h2 = cv2.boundingRect(maxc2)
    if paths2.index(city) % 2 == 0 and paths2.index(city) != 0:
        x2prim = x2 - 2*paths2.index(city)*10
        y2prim = y2
    elif paths2.index(city) % 2 != 0:
        y2prim = y2 - paths2.index(city)*10
        x2prim = x2
    else:
        x2prim = x2 - 40
        y2prim = y2 - 40
        w2 = w2 - 20
        h2 = h2 - 20
    cv2.rectangle(city_img, (x2prim, y2prim), (x2 + w2, y2 + h2), (255, 255, 0), 8)
    city_img = cv2.resize(city_img, (1280, 720))
    cv2.imshow(city, city_img)

cv2.waitKey(0)
cv2.destroyAllWindows()
